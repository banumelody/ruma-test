package id.web.banu.rumatest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class TabViewFragment extends Fragment {

    String contentString;

    public TabViewFragment(String contentString) {
        this.contentString = contentString;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_tab_view, container, false);

        TextView textContent = (TextView) view.findViewById(R.id.textContent);

        textContent.setText(contentString);

        return view;


    }
}
