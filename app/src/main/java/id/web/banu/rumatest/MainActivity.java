package id.web.banu.rumatest;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;


public class MainActivity extends AppCompatActivity {

    String[][] pageContents = {
            {"A", "B", "C"},
            {"D", "E"},
            {"F"}
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        TabHost tabHost = (TabHost)findViewById(R.id.tabHost);
        assert tabHost != null;
        tabHost.setup();

        //Tab 1
        LinearLayout buttonsLayout = (LinearLayout)findViewById(R.id.buttonsLayout);
        final RadioGroup radioGroup = new RadioGroup(getBaseContext());
        radioGroup.setOrientation(LinearLayout.HORIZONTAL);
        for (int x=0; x<pageContents[0].length; x++) {
            final RadioButton rb = new RadioButton(getBaseContext());
            rb.setId(x+100);
            if (x==0) rb.setChecked(true);
            radioGroup.addView(rb);
        }

        assert buttonsLayout != null;
        buttonsLayout.addView(radioGroup);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), pageContents[0]);
        assert viewPager != null;
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                radioGroup.check(position+100);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                viewPager.setCurrentItem(radioGroup.getCheckedRadioButtonId()-100);
            }
        });

        TabHost.TabSpec spec = tabHost.newTabSpec("Tab One");
        spec.setContent(R.id.linearLayout);
        spec.setIndicator("Tab One");
        tabHost.addTab(spec);

        //Tab 2
        LinearLayout buttonsLayout2 = (LinearLayout)findViewById(R.id.buttonsLayout2);
        final RadioGroup radioGroup2 = new RadioGroup(getBaseContext());
        radioGroup2.setOrientation(LinearLayout.HORIZONTAL);
        for (int x=0; x<pageContents[1].length; x++) {
            final RadioButton rb = new RadioButton(getBaseContext());
            rb.setId(x+200);
            if (x==0) rb.setChecked(true);
            radioGroup2.addView(rb);
        }


        assert buttonsLayout2 != null;
        buttonsLayout2.addView(radioGroup2);

        final ViewPager viewPager2 = (ViewPager) findViewById(R.id.pager2);
        final PagerAdapter adapter2 = new PagerAdapter
                (getSupportFragmentManager(), pageContents[1]);
        assert viewPager2 != null;
        viewPager2.setAdapter(adapter2);

        viewPager2.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                radioGroup2.check(position+200);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                viewPager2.setCurrentItem(checkedId-200);
            }
        });

        spec = tabHost.newTabSpec("Tab Two");
        spec.setContent(R.id.linearLayout2);
        spec.setIndicator("Tab Two");
        tabHost.addTab(spec);

        //Tab 3
        final ViewPager viewPager3 = (ViewPager) findViewById(R.id.pager3);
        final PagerAdapter adapter3 = new PagerAdapter
                (getSupportFragmentManager(), pageContents[2]);
        assert viewPager3 != null;
        viewPager3.setAdapter(adapter3);

        spec = tabHost.newTabSpec("Tab Three");
        spec.setContent(R.id.linearLayout3);
        spec.setIndicator("Tab Three");
        tabHost.addTab(spec);

    }


}