package id.web.banu.rumatest;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by banumelody on 6/11/16.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
    int numOfTabs;
    String[] contents;

    public PagerAdapter(FragmentManager fm, String[] contents) {
        super(fm);
        this.contents = contents;
        this.numOfTabs = contents.length;
    }

    @Override
    public Fragment getItem(int position) {

        return new TabViewFragment(contents[position]);
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
